TARGET		= ee
TEMPLATE	= app

HEADERS     = 

SOURCES		= 

SOURCES	   += main.cpp

#RESOURCES     = icons.qrc

#FORMS = 

INCLUDEPATH *= $$PWD/
DEPENDPATH  *= $$PWD/


#========= Dependency ==============
					
#	--- Qt ---
QT  += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
#QT += svg

#	--- External ---


#====== Convenient config =========
DEBUGCONFIG = false
include($$(SOFT_LIB)/MyPri/stdconfig.pri)
