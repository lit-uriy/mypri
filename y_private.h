/*!
 *	\file	y_private.h
 */
#ifndef Y_PRIVATE_H
#define Y_PRIVATE_H

#define Y_DECLARE_PRIVATE(Class) \
    Class##Private* d; \
    friend class Class##Private;

#define Y_DECLARE_PUBLIC(Class) \
    Class* q; \
    friend class Class;
    
#define Y_D(Class) Class##Private * const d
#define Y_Q(Class) Class * const q

#define Y_INIT_Q_D(Class) \
    d = new Class##Private; \
    d->q = this;

#endif //Y_PRIVATE_H

