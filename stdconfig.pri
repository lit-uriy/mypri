######################################################################
# HowTo USE
######################################################################
# In pro-file:
# TEMPDIR - set Temporary Directory (default: ./tmp)
# DESTDIR - set Destination Directory (default for libs: ./lib; for app: ./build)
# TRDIR - set Translations Directory (optionally, need if HARDCODED += translationdir)
# VERSION - set Version string (optionally, need if HARDCODED += version)
#
# DEBUGCONFIG = false  - off pro-file debuging  (optionally)
#
# HARDCODED += <name> - hardcoded (defined) <name> in DEFINE  (optionally)
#   here is <name> suported:
#       target		- #define TARGET = $${TARGET}
#	translationdir	- #define TRDIR = $${TRDIR}
#	version     	- #define VERSION = $${VERSION}, need define VERSION in PRO-file
#	git_version     - #define GIT_SHA_VERSION = $${GIT_SHA_VERSION}, need Git
#
# include($$(LIBS)/MyPri/stdconfig.pri)
#
#
######################################################################

message("If you need debug pri-file, then write: DEBUGCONFIG = true")



#=========== Temporary directory ========
isEmpty(TEMPDIR){
	isEmpty(TMPDIR){
		TMPDIR = ./tmp
                contains(DEBUGCONFIG, true){
                    message(Auto set TEMPDIR = $${TMPDIR})
                }
	}
}else{
	TMPDIR = $$TEMPDIR
}

TMP_DIR = $$TMPDIR

#=========== Destination directory ========
isEmpty(DESTDIR){
        DESTDIR_ = .
	contains(TEMPLATE, lib){
                DESTDIR = $${DESTDIR_}/lib
	}else{
                DESTDIR = $${DESTDIR_}/build
	}
        contains(DEBUGCONFIG, true){
            message(Auto set DESTDIR = $${DESTDIR})
        }
}




#=========== Terget Name ========
TS_BASENAME = $${TARGET}

CONFIG (debug, debug|release) {
	contains(TEMPLATE, lib){
		TARGET = $$qtLibraryTarget($$TARGET)
	}else{
		TARGET  	= $$join(TARGET,,,.d)
	}
	OBJECTS_DIR = $${TMP_DIR}/debug
	# Console added in debug mode
        CONFIG 	   += console
}else{
        OBJECTS_DIR = $${TMP_DIR}/release
}

#=========== Std pathes ========
UI_DIR         = $${TMP_DIR}/uic
UI_SOURCES_DIR = $${TMP_DIR}/uic
UI_HEADERS_DIR = $${TMP_DIR}/uic
MOC_DIR        = $${TMP_DIR}/moc
RCC_DIR		   = $${TMP_DIR}/rcc

#=========== build all if debug_and_release ========
contains(CONFIG, debug_and_release) {
	CONFIG 	   += build_all
}


#================== HARDCODED NAMES ===============
contains(HARDCODED, target) {
	TARGETTMP = '\\"$${TARGET}\\"'
	DEFINES  += TARGET=\"$${TARGETTMP}\"
}

contains(HARDCODED, translationdir) {
	isEmpty(TRDIR){
		TRDIR = $$DESTDIR
                contains(DEBUGCONFIG, true){
                    message(Auto set TRDIR = $${TRDIR})
                }
	}
	TRDIR_TMP = '\\"$${TRDIR}\\"'
	DEFINES  += TRDIR=\"$${TRDIR_TMP}\"

}

contains(HARDCODED, translations) {
	isEmpty(TRDIR){
		TRDIR = $$DESTDIR
                contains(DEBUGCONFIG, true){
                    message(Auto set TRDIR = $${TRDIR})
                }
	}
	TRDIR_TMP = '\\"$${TRDIR}\\"'
	DEFINES  += TRDIR=\"$${TRDIR_TMP}\"

	TS_BASENAME_TMP = '\\"$${TS_BASENAME}\\"'
	DEFINES  += TS_BASENAME=\"$${TS_BASENAME_TMP}\"
	

}

contains(HARDCODED, version) {
	isEmpty(VERSION){
		error (Version is HARDCODED but is Empty)
	}else{
		VER_TMP = '\\"$${VERSION}\\"'
		DEFINES  += VERSION=\"$${VER_TMP}\"
	}
}

contains(HARDCODED, git_version) {
# нужно запросить у git-а текущий SHA1
    UNAME = $$system(git -C $$_PRO_FILE_PWD_ rev-parse HEAD)
    message(GITVERSION = $${UNAME})
        isEmpty(UNAME){
                error (Git Version is HARDCODED but is Empty)
        }else{
                UNAME_TMP = '\\"$${UNAME}\\"'
                DEFINES  += GIT_SHA_VERSION=\"$${UNAME_TMP}\"
        }
}

!isEmpty(includes.files){
    isEmpty(includes.path){
        includes.path = $$OUT_PWD/include
    }
    INSTALLS += includes
    DISTFILES += includes.files
}

#========= Makefile Target fo preprocessoring =======
#topreprocessor1.target	= %.i
#topreprocessor1.depends	= $(MAKEFILE).Debug FORCE
#topreprocessor1.commands	= $(MAKE) -f $(MAKEFILE).Debug $@

#QMAKE_EXTRA_TARGETS	   += topreprocessor1

#CONFIG (debug, debug|release) {
#	topreprocessorcpp.target	= %.i
#	topreprocessorcpp.depends	= %.cpp
#	topreprocessorcpp.commands	= $(CXX) -E $(CXXFLAGS) $(INCPATH) $< -o $@
#	QMAKE_EXTRA_TARGETS	   += topreprocessorcpp

#	topreprocessorh.target	= %.i
#	topreprocessorh.depends	= %.h
#	topreprocessorh.commands	= $(CXX) -E $(CXXFLAGS) $(INCPATH) $< -o $@
#	QMAKE_EXTRA_TARGETS	   += topreprocessorh

#	toprprocessorc.target	= %.i
#	toprprocessorc.depends	= %.c
#	toprprocessorc.commands	= $(CXX) -E $(CXXFLAGS) $(INCPATH) $< -o $@
#	QMAKE_EXTRA_TARGETS	   += toprprocessorc
#}
#=================== DEBUG PRI ======================
contains(DEBUGCONFIG, true){
    CONFIG (debug, debug|release) {
        message(-------------- DEBUG --------------------)
    }
    CONFIG (release, debug|release) {
        message(-------------- RELEASE --------------------)
    }
        message(CONFIG-POST = $${CONFIG})
	message(DESTDIR=$${DESTDIR})
	message(TARGET=$${TARGET})
#	message(TMPDIR=$${TMPDIR})
#	message(TRDIR=$${TRDIR})
	message(OBJECTS_DIR=$${OBJECTS_DIR})
	message(UI_DIR=$${UI_DIR})
#	message(INCLUDEPATH = $${INCLUDEPATH})
#	message(QMAKE_LIBDIR = $${QMAKE_LIBDIR})
#	message(LIBS = $${LIBS})
	!isEmpty(HARDCODED){
		message(HARDCODED = $${HARDCODED})
		message(DEFINES = $${DEFINES})
	}
#	!isEmpty(includes.files){
#		message(includes_files=$${includes.files})
#		message(includes_path=$${includes.path})
#	}
#	message(HEADERS=$${HEADERS})
#	message(SOURCES=$${SOURCES})
	message(===================================)
}
