/*!
 \file main.cpp
 \brief Основной файл программы.
 */

#include <QApplication>
#include <QStyleFactory>
#include <QStyle>
#include <QLocale>
#include <QTranslator>
#include <QDir>

#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
#include <QTextCodec>
#endif

#include "mainwindow.h"

int main(int argc, char *argv[])
{
    //Q_INIT_RESOURCE(mdi);

  
	QApplication app(argc, argv);

	// Устанавливаем кодеки    
#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));// Для функций перевода tr()
	//QTextCodec::setCodecForCStrings(QTextCodec::codecForLocale());
#endif

	// Устанавливаем свойства программы (понадобится для настроек)
	//QCoreApplication::setOrganizationName(QObject::trUtf8("ЗАО \"ННН\""));
	//QCoreApplication::setApplicationName(QObject::trUtf8("ЭСТОП АСУ"));

	// Устанавливаем стили
    /*
	QStyle *style = QStyleFactory::create("windows");//Cleanlooks Plastique windowsxp
	if (style){
		QApplication::setStyle(style);
		qApp->setPalette(style->standardPalette());
	}
    */
	
	// Устанавливаем превод библиотек Qt
	QString locale = QLocale::system().name();	// запрос языка (например: "ru")
	QTranslator *qttr = new QTranslator; 
	if (qttr->load(QString("qt_") + locale))
		qApp->installTranslator(qttr);         


	Mainwindow mainWin;

	//mainWin.resize(750, 550);
	mainWin.show();

    return app.exec();
}
